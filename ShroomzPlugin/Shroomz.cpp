#include "UnityPluginInterface.h"

#if UNITY_WIN
#   include <Windows.h>
#	if SUPPORT_D3D9
#		include <d3d9.h>
#	endif
# else
#   include <stdio.h>
#endif

#include <math.h>
#include <string.h>

#if SUPPORT_OPENGL
#	include <gl/glew.h>
#elif SUPPORT_OPENGLES
#	if UNITY_IPHONE
#		include <OpenGLES/ES2/gl.h>
#	elif UNITY_ANDROID
#		include <GLES2/gl2.h>
#	endif
#endif

static void DebugLog(const char *str)
{
#if UNITY_WIN
	OutputDebugStringA(str);
#else
	printf("%s", str);
#endif
}

struct Vertex
{
	float x, y, z, s, t;
};

#if UNITY_WIN
#   pragma pack(push, 1)
#endif
struct Shroom
{
    float x, y, z,
    scaleX, scaleY, angle,
    r, g, b, a,
    s, t, w, h;
}
#ifndef UNITY_WIN
__attribute__((packed))
#endif
;
#if UNITY_WIN
#   pragma pack(push, 1)
#endif

//#define DO_CULL_FACE

#define ALPHA_TEST_TRESHOLD_F 0.25f
#define ALPHA_TEST_TRESHOLD_STR "0.25"

#ifdef SUPPORT_OPENGLES

#   define SHADER_ATTRIB_POS 1
#   define SHADER_ATTRIB_UV 2

#   define VSHADER_SRC \
	"attribute highp vec3 pos;\n"\
	"attribute highp vec2 uv;\n"\
	"varying highp vec2 st;"\
	"uniform highp mat4 worldMatrix;\n"\
	"uniform highp mat4 projMatrix;\n"\
	"void main()\n"\
	"{\n"\
	"	st = uv;\n"\
	"	gl_Position = (projMatrix * worldMatrix) * vec4(pos, 1);\n"\
	"}\n"

#   define FSHADER_NORMAL_SRC \
	"varying highp vec2 st;"\
	"uniform sampler2D texSampler;"\
	"void main()\n"\
	"{\n"\
	"	gl_FragColor = texture2D(texSampler, st);\n"\
	"}\n"

#   define FSHADER_DISCARD_SRC(treshold)\
	"varying highp vec2 st;"\
	"uniform sampler2D texSampler;"\
	"void main()\n"\
	"{\n"\
	"   lowp vec4 col = texture2D(texSampler, st);\n"\
	"   if (col.a < " treshold ")\n"\
	"       discard;\n"\
	"   else\n"\
	"       gl_FragColor = texture2D(texSampler, st);\n"\
	"}\n"

static GLuint g_CurProgram = 0, g_ProgNormal, g_ProgDiscard;
static GLint g_TextureUIdx[2], g_WorldMatrixUIdx[2], g_ProjMatrixUIdx[2];

static GLuint CreateShader(GLenum type, const char *text)
{
	GLuint ret = glCreateShader(type);
	glShaderSource(ret, 1, &text, NULL);
	glCompileShader(ret);
	
#   ifdef DEBUG
	int logLen;
	glGetShaderiv(ret, GL_INFO_LOG_LENGTH, &logLen);
	
	DebugLog("Begin of shader compilation output:\n");
	if (logLen > 0)
	{
		GLchar *log = new GLchar[logLen];
		glGetShaderInfoLog(ret, logLen, &logLen, log);
		DebugLog(log);
		delete[] log;
	}
	else
		DebugLog("Compiled Succesfully");
	DebugLog("\nEnd\n");
#   endif
	
	return ret;
}

static GLuint CreateProgram(GLuint vShader, GLuint pShadert)
{
	GLuint ret = glCreateProgram();
	
	glBindAttribLocation(ret, SHADER_ATTRIB_POS, "pos");
	glBindAttribLocation(ret, SHADER_ATTRIB_UV, "uv");
	
	glAttachShader(ret, vShader);
	glAttachShader(ret, pShadert);
	
	glLinkProgram(ret);
	
#   ifdef DEBUG
	int logLen;
	glGetProgramiv(ret, GL_INFO_LOG_LENGTH, &logLen);
	
	DebugLog("Begin of program compilation output:\n");
	if (logLen > 0)
	{
		GLchar *log = new GLchar[logLen];
		glGetProgramInfoLog(ret, logLen, &logLen, log);
		DebugLog(log);
		delete[] log;
	}
	else
		DebugLog("Compiled & Linked Succesfully");
	DebugLog("\nEnd\n");
#   endif
	
	return ret;
}

#endif

static Vertex *g_Data = NULL;

static GLuint g_Vbo = -1;
static void CreateVBO(GLuint size, GLvoid *data, bool dynamic)
{
#ifdef SUPPORT_OPENGL
	if (!GLEW_ARB_vertex_buffer_object)
		return;
#endif
	
	if (g_Vbo == -1)
	{
		glGenBuffers(1, &g_Vbo);
		glBindBuffer(GL_ARRAY_BUFFER, g_Vbo);
		glBufferData(GL_ARRAY_BUFFER, size, (GLvoid*)data, dynamic ? GL_DYNAMIC_DRAW : GL_STATIC_DRAW);
	}
	else
	{
		glBindBuffer(GL_ARRAY_BUFFER, g_Vbo);
		glBufferData(GL_ARRAY_BUFFER, size, NULL, dynamic ? GL_DYNAMIC_DRAW : GL_STATIC_DRAW);
		glBufferSubData(GL_ARRAY_BUFFER, 0, size, data);
	}
}

static void FreeVBO()
{
	if (g_Vbo == -1)
		return;
	
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glDeleteBuffers(1, &g_Vbo);
	
	g_Vbo = -1;
}

enum RENDER_MODE
{
	RE_DIRECT = 0,
	RE_DYNAMIC,
	RE_STATIC
};

static RENDER_MODE g_Mode = RE_DIRECT;
static unsigned g_Capacity = -1;
extern "C" bool EXPORT_API ShroomzInitialize(unsigned capacity, RENDER_MODE mode)
{
	if (g_Capacity != -1)
		return false;
	
	g_Capacity = capacity;
	g_Data = new Vertex[6 * g_Capacity];
	
	g_Mode = mode;
	if (g_Mode != RE_DIRECT)
		CreateVBO(6 * g_Capacity * sizeof(Vertex), (GLvoid *)g_Data, g_Mode == RE_DYNAMIC);
	
	return true;
}

static void *g_TexturePointer = NULL;
extern "C" void EXPORT_API ShroomzSetTexture(void *texturePtr)
{
	g_TexturePointer = texturePtr;
}

static inline void quickSort(Shroom *shroomz, int first, int last)
{
    int pivot = first;
    
    if (first < last)
    {
        Shroom pivotElement = shroomz[first];
        
        for (int i = first + 1; i <= last; ++i)
            if (shroomz[i].z > pivotElement.z) // If you want to sort array in the other order, change ">" to "<=".
            {
                ++pivot;
                Shroom t = shroomz[i];
                shroomz[i] = shroomz[pivot];
                shroomz[pivot] = t;
            }
        
        Shroom t = shroomz[pivot];
        shroomz[pivot] = shroomz[first];
        shroomz[first] = t;
        
        quickSort(shroomz, first, pivot - 1);
        quickSort(shroomz, pivot + 1, last);
    }
}

static unsigned g_RenderCount = -1;
extern "C" void EXPORT_API ShroomzUpdate(Shroom *shroomz, unsigned count, bool doSort)
{
    if (!shroomz || count == 0 || count == -1)
        return;
        
	if (doSort)
        quickSort(shroomz, 0, count - 1);
	
	for (unsigned i = 0; i < count; ++i)
	{
		const unsigned idx = i * 6;
		
        const float cos = cosf(shroomz[i].angle), sin = sinf(shroomz[i].angle),
		&x = shroomz[i].x, oxc = 0.5f * shroomz[i].scaleX * cos, oxs = 0.5f * shroomz[i].scaleX * sin,
		&y = shroomz[i].y, oyc = 0.5f * shroomz[i].scaleY * cos, oys = 0.5f * shroomz[i].scaleY * sin;
		
		g_Data[idx + 0].x = (-oxc + oys) + x;
		g_Data[idx + 1].x = (-oxc - oys) + x;
		g_Data[idx + 2].x = ( oxc + oys) + x;
		g_Data[idx + 3].x = (-oxc - oys) + x;
		g_Data[idx + 4].x = ( oxc - oys) + x;
		g_Data[idx + 5].x = ( oxc + oys) + x;
		
		g_Data[idx + 0].y = (-oxs - oyc) + y;
		g_Data[idx + 1].y = (-oxs + oyc) + y;
		g_Data[idx + 2].y = ( oxs - oyc) + y;
		g_Data[idx + 3].y = (-oxs + oyc) + y;
		g_Data[idx + 4].y = ( oxs + oyc) + y;
		g_Data[idx + 5].y = ( oxs - oyc) + y;
		
		const float &z = shroomz[i].z;
		g_Data[idx + 0].z = z;
		g_Data[idx + 1].z = z;
		g_Data[idx + 2].z = z;
		g_Data[idx + 3].z = z;
		g_Data[idx + 4].z = z;
		g_Data[idx + 5].z = z;
		
        const float &s = shroomz[i].s, t = 1.f - shroomz[i].t,
        w = s + shroomz[i].w, h = t - shroomz[i].h;
		
        g_Data[idx + 0].s = s;
		g_Data[idx + 1].s = s;
		g_Data[idx + 2].s = w;
		g_Data[idx + 3].s = s;
		g_Data[idx + 4].s = w;
		g_Data[idx + 5].s = w;
		
        g_Data[idx + 0].t = h;
        g_Data[idx + 1].t = t;
        g_Data[idx + 2].t = h;
        g_Data[idx + 3].t = t;
        g_Data[idx + 4].t = t;
        g_Data[idx + 5].t = h;
	}
	
	g_RenderCount = count;
	
	if (g_Mode != RE_DIRECT)
		CreateVBO(6 * g_Capacity * sizeof(Vertex), (GLvoid *)g_Data, g_Mode == RE_DYNAMIC);
}

static bool g_DoBlending = false;
extern "C" void EXPORT_API ShroomzToggleBlending(bool doBlending)
{
	g_DoBlending = doBlending;
#ifdef SUPPORT_OPENGLES
	if (g_DoBlending)
		g_CurProgram = g_ProgNormal;
	else
		g_CurProgram = g_ProgDiscard;
#endif
}

static float g_WorldMatrix[16], g_ProjectionMatrix[16];
extern "C" void EXPORT_API ShroomzSetMatrices(const float *worldMatrix, const float *projectionMatrix)
{
	memcpy(g_WorldMatrix, worldMatrix, 16 * sizeof(float));
	memcpy(g_ProjectionMatrix, projectionMatrix, 16 * sizeof(float));
}

extern "C" void EXPORT_API ShroomzFree()
{
	if (g_Capacity == -1)
		return;
	
#ifdef SUPPORT_OPENGLES
	glUseProgram(0);
	glDeleteProgram(g_ProgNormal);
	glDeleteProgram(g_ProgDiscard);
#endif
	
	if (g_Mode != RE_DIRECT)
		FreeVBO();
	
	delete[] g_Data;
	
	g_TexturePointer = NULL;
	g_DoBlending = false;
	g_Capacity = -1;
	g_Mode = RE_DIRECT;
}

// --------------------------------------------------------------------------
// UnitySetGraphicsDevice
static int g_DeviceType = -1;
#if SUPPORT_D3D9
static IDirect3DDevice9 *g_D3D9Device;
static IDirect3DVertexBuffer9* g_D3D9DynamicVB;
#endif
extern "C" void EXPORT_API UnitySetGraphicsDevice(void *device, int deviceType, int eventType)
{
	g_DeviceType = -1;
	
#if SUPPORT_D3D9
	if (deviceType == kGfxRendererD3D9)
	{
		DebugLog ("Set D3D9 graphics device\n");
		g_DeviceType = deviceType;
		
		g_D3D9Device = (IDirect3DDevice9*)device;

		switch (eventType)
		{
		case kGfxDeviceEventInitialize:
		case kGfxDeviceEventAfterReset:
			if (!g_D3D9DynamicVB)
				g_D3D9Device->CreateVertexBuffer(1024, D3DUSAGE_WRITEONLY | D3DUSAGE_DYNAMIC, 0, D3DPOOL_DEFAULT, &g_D3D9DynamicVB, NULL);
			break;
		case kGfxDeviceEventBeforeReset:
		case kGfxDeviceEventShutdown:
			g_D3D9DynamicVB->Release();
			g_D3D9DynamicVB = NULL;
			break;
		}
	}
#endif

#if SUPPORT_OPENGL
	if(deviceType == kGfxRendererOpenGL)
	{
		DebugLog("Set OpenGL graphics device\n");
		g_DeviceType = deviceType;
		
		GLenum glew_res = glewInit();
		
		if (glew_res != GLEW_OK)
			DebugLog("Error initializing GLEW\n");
	}
#endif
	
#ifdef SUPPORT_OPENGLES
	if(deviceType == kGfxRendererOpenGLES20Mobile)
	{
		DebugLog("Set OpenGLES 2.0 device\n");
		g_DeviceType = deviceType;
		
		GLuint vShader, fShaderNormal, fShaderDiscard;
		vShader = CreateShader(GL_VERTEX_SHADER, VSHADER_SRC);
		fShaderNormal = CreateShader(GL_FRAGMENT_SHADER, FSHADER_NORMAL_SRC);
		fShaderDiscard = CreateShader(GL_FRAGMENT_SHADER, FSHADER_DISCARD_SRC(ALPHA_TEST_TRESHOLD_STR));
		
#       undef VSHADER_SRC
#       undef FSHADER_NORMAL_SRC
#       undef FSHADER_DISCARD_SRC
		
		g_ProgNormal = CreateProgram(vShader, fShaderNormal);
		g_ProgDiscard = CreateProgram(vShader, fShaderDiscard);
		
		glDeleteShader(vShader);
		glDeleteShader(fShaderNormal);
		glDeleteShader(fShaderDiscard);
		
		// Uniforms will be identical for both programs (on real device) but let's do it safe way.
		g_TextureUIdx[0] = glGetUniformLocation(g_ProgNormal, "texSampler");
		g_WorldMatrixUIdx[0] = glGetUniformLocation(g_ProgNormal, "worldMatrix");
		g_ProjMatrixUIdx[0] = glGetUniformLocation(g_ProgNormal, "projMatrix");
		
		g_TextureUIdx[1] = glGetUniformLocation(g_ProgDiscard, "texSampler");
		g_WorldMatrixUIdx[1] = glGetUniformLocation(g_ProgDiscard, "worldMatrix");
		g_ProjMatrixUIdx[1] = glGetUniformLocation(g_ProgDiscard, "projMatrix");
		
		g_CurProgram = g_ProgDiscard;
	}
	else
		DebugLog("Wrong OpenGLES device\n");
#endif
}

// --------------------------------------------------------------------------
// UnityRenderEvent
// This will be called for GL.IssuePluginEvent script calls; eventID will
// be the integer passed to IssuePluginEvent.
extern "C" void EXPORT_API UnityRenderEvent(int eventID)
{
	if (g_DeviceType == -1)
		return;
	
	const unsigned short first = (eventID >> 16) & 0xFFFF, count = eventID & 0xFFFF;
   
#if SUPPORT_D3D9
	if (g_DeviceType == kGfxRendererD3D9)
	{
#ifdef DO_CULL_FACE
		//ToDo
#else
		g_D3D9Device->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
#endif
		g_D3D9Device->SetRenderState(D3DRS_LIGHTING, FALSE);
		g_D3D9Device->SetRenderState(D3DRS_ZFUNC, D3DCMP_LESSEQUAL);
		g_D3D9Device->SetRenderState(D3DRS_ZWRITEENABLE, FALSE);

		if (g_DoBlending)
		{
			g_D3D9Device->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
			g_D3D9Device->SetRenderState(D3DRS_ALPHATESTENABLE, FALSE);
		}
		else
		{
			g_D3D9Device->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
			g_D3D9Device->SetRenderState(D3DRS_ALPHATESTENABLE, TRUE);
		}

		const float identityMatrix[16] = {1.f, 0.f, 0.f, 0.f, 0.f, 1.f, 0.f, 0.f, 0.f, 0.f, 1.f, 0.f, 0.f, 0.f, 0.f, 1.f};

		g_D3D9Device->SetTransform(D3DTS_WORLD, (const D3DMATRIX*)g_WorldMatrix);
		g_D3D9Device->SetTransform(D3DTS_VIEW, (const D3DMATRIX*)identityMatrix);
		g_D3D9Device->SetTransform(D3DTS_PROJECTION, (const D3DMATRIX*)g_ProjectionMatrix);

		g_D3D9Device->SetFVF (D3DFVF_XYZ | D3DFVF_TEX1);

		g_D3D9Device->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_SELECTARG1);
		g_D3D9Device->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_CURRENT);
		g_D3D9Device->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);
		g_D3D9Device->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_CURRENT);
		g_D3D9Device->SetTextureStageState(1, D3DTSS_COLOROP, D3DTOP_DISABLE);
		g_D3D9Device->SetTextureStageState(1, D3DTSS_ALPHAOP, D3DTOP_DISABLE);

		void *vbPtr;
		g_D3D9DynamicVB->Lock(0, 0, &vbPtr, D3DLOCK_DISCARD);
		memcpy (vbPtr, g_Data, sizeof(Vertex) * 6 * g_Capacity);
		g_D3D9DynamicVB->Unlock();
		g_D3D9Device->SetStreamSource(0, g_D3D9DynamicVB, 0, sizeof(Vertex));

		g_D3D9Device->SetTexture(0, (IDirect3DTexture9*)g_TexturePointer);

		g_D3D9Device->DrawPrimitive(D3DPT_TRIANGLELIST, first * 6, count * 2);
	}
#endif

#if SUPPORT_OPENGL || SUPPORT_OPENGLES
	if (g_DeviceType == kGfxRendererOpenGL || g_DeviceType == kGfxRendererOpenGLES20Mobile)
	{
		glDepthFunc(GL_LEQUAL);
		glDepthMask(GL_FALSE);
		glEnable(GL_DEPTH_TEST);
		
#ifdef DO_CULL_FACE
		glCullFace(GL_BACK);
		glFrontFace(GL_CW);
		glEnable(GL_CULL_FACE);
#else
		glDisable(GL_CULL_FACE);
#endif
		
		if (g_DoBlending)
		{
#   ifdef SUPPORT_OPENGL
			glDisable(GL_ALPHA_TEST);
#   endif
			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		}
		else
		{
			glDisable(GL_BLEND);
#   ifdef SUPPORT_OPENGL
			glEnable(GL_ALPHA_TEST);
			glAlphaFunc(GL_GREATER, ALPHA_TEST_TRESHOLD_F);
#   endif
		}
		
#	ifdef SUPPORT_OPENGL
		if (GLEW_ARB_multitexture)
#	endif
			glActiveTexture(GL_TEXTURE0);
		
#	ifdef SUPPORT_OPENGL
		// Obsolete but in case.
		glDisable(GL_LIGHTING);
		glColor4f(1.f, 1.f, 1.f, 1.f);
		
		glEnable(GL_TEXTURE_2D);
		glMatrixMode(GL_TEXTURE);
		glLoadIdentity();
		glMatrixMode(GL_PROJECTION);
		glLoadMatrixf(g_ProjectionMatrix);
		glMatrixMode(GL_MODELVIEW);
		glLoadMatrixf(g_WorldMatrix);
#	elif SUPPORT_OPENGLES
		const GLint uidx = g_DoBlending ? 0 : 1;
		glUseProgram(g_CurProgram);
		glUniform1i(g_TextureUIdx[uidx], 0);
		glUniformMatrix4fv(g_WorldMatrixUIdx[uidx], 1, GL_FALSE, g_WorldMatrix);
		glUniformMatrix4fv(g_ProjMatrixUIdx[uidx], 1, GL_FALSE, g_ProjectionMatrix);
#	endif
		
		glBindTexture(GL_TEXTURE_2D, (GLuint)(size_t)(g_TexturePointer));
		
#ifdef SUPPORT_OPENGL
		if (!GLEW_ARB_vertex_buffer_object) {
#endif
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
			
			if (g_Vbo != -1)
				glBindBuffer(GL_ARRAY_BUFFER, g_Vbo);
			else
				glBindBuffer(GL_ARRAY_BUFFER, 0);
			
#	ifdef SUPPORT_OPENGL
		}
		
		glDisableClientState(GL_COLOR_ARRAY);
		
#	ifdef SUPPORT_OPENGL
        if (GLEW_ARB_multitexture)
#	endif
            glClientActiveTexture(GL_TEXTURE0);
        
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		glTexCoordPointer(2, GL_FLOAT, sizeof(Vertex), g_Vbo != -1 ? (GLvoid *)(3 * sizeof(float)) : &g_Data[0].s);
		
		glEnableClientState(GL_VERTEX_ARRAY);
		glVertexPointer(3, GL_FLOAT, sizeof(Vertex), g_Vbo != -1 ? 0 : &g_Data[0].x);
#	elif SUPPORT_OPENGLES
		glEnableVertexAttribArray(SHADER_ATTRIB_POS);
		glVertexAttribPointer(SHADER_ATTRIB_POS, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), g_Vbo != -1 ? 0 : g_Data);
		
		glEnableVertexAttribArray(SHADER_ATTRIB_UV);
		glVertexAttribPointer(SHADER_ATTRIB_UV, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), g_Vbo != -1 ? (GLvoid *)(3 * sizeof(float)) : &g_Data[0].s);
#	endif
		
		glDrawArrays(GL_TRIANGLES, first * 6, count * 6);
	}
#endif
}
