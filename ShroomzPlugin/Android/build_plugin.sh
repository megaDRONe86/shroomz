#!/bin/sh
echo ""
echo "Compiling NativeCode.c..."
export ANDROID_NDK_ROOT=/Developer/adt-bundle-mac-x86_64-20140702/android-ndk-r10c
$ANDROID_NDK_ROOT/ndk-build NDK_PROJECT_PATH=. NDK_APPLICATION_MK=Application.mk $*
mv libs/armeabi/libnative.so ../../UnityProject/Assets/Plugins/Android/libShroomz.so

echo ""
echo "Cleaning up / removing build folders..."  #optional..
rm -rf libs
rm -rf obj

echo ""
echo "Done!"
