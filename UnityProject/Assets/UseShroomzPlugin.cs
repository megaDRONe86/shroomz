using UnityEngine;
using System;
using System.Collections;
using System.Runtime.InteropServices;

public class UseShroomzPlugin : MonoBehaviour
{
	private enum RENDER_MODE
	{
		DIRECT = 0,  // draw directly from RAM
		DYNAMIC,     // same as above but through dynamic VBO
		STATIC       // copy data to VRAM and draw from VRAM directly
	};

	private struct Shroom
	{
		private const uint DATA_BLOCK = 14;
		private static float[] data;
		private uint idx;

#if UNITY_IPHONE && !UNITY_EDITOR
	[DllImport("__Internal")]
#else
	[DllImport("Shroomz")]
#endif
	private static extern bool ShroomzInitialize(uint capacity, RENDER_MODE mode);

#if UNITY_IPHONE && !UNITY_EDITOR
	[DllImport("__Internal")]
#else
	[DllImport("Shroomz")]
#endif
	private static extern void ShroomzFree();

#if UNITY_IPHONE && !UNITY_EDITOR
	[DllImport("__Internal")]
#else
	[DllImport("Shroomz")]
#endif
	private static extern void ShroomzUpdate([MarshalAs(UnmanagedType.LPArray)] float[] shroomz, uint count, [MarshalAs(UnmanagedType.U1)]bool doSort);

#if UNITY_IPHONE && !UNITY_EDITOR
	[DllImport("__Internal")]
#else
	[DllImport("Shroomz")]
#endif
	private static extern void ShroomzSetTexture(System.IntPtr texture);

#if UNITY_IPHONE && !UNITY_EDITOR
	[DllImport("__Internal")]
#else
	[DllImport("Shroomz")]
#endif
	private static extern void ShroomzToggleBlending([MarshalAs(UnmanagedType.U1)]bool doBlending);

#if UNITY_IPHONE && !UNITY_EDITOR
	[DllImport("__Internal")]
#else
	[DllImport("Shroomz")]
#endif
	private static extern void ShroomzSetMatrices(ref Matrix4x4 worldMatrix, ref Matrix4x4 projectionMatrix);

	public static bool Initialize(uint capacity, RENDER_MODE mode)
	{
		data = new float[DATA_BLOCK * capacity];

		for (uint i = 0; i < capacity; ++i)
		{
			data[i * DATA_BLOCK ]     = 0.0F;
			data[i * DATA_BLOCK + 1]  = 0.0F;
			data[i * DATA_BLOCK + 2]  = 0.0F;
			data[i * DATA_BLOCK + 3]  = 1.0F;
			data[i * DATA_BLOCK + 4]  = 1.0F;
			data[i * DATA_BLOCK + 5]  = 0.0F;
			data[i * DATA_BLOCK + 6]  = 1.0F;
			data[i * DATA_BLOCK + 7]  = 1.0F;
			data[i * DATA_BLOCK + 8]  = 1.0F;
			data[i * DATA_BLOCK + 9]  = 1.0F;
			data[i * DATA_BLOCK + 10] = 0.0F;
			data[i * DATA_BLOCK + 11] = 0.0F;
			data[i * DATA_BLOCK + 12] = 1.0F;
			data[i * DATA_BLOCK + 13] = 1.0F;
		}

		return ShroomzInitialize(capacity, mode);
	}

	public static void Free()
	{
		data = null;
		ShroomzFree();
	}

	public static void Update(uint count, bool doSort)
	{
		ShroomzUpdate(data, count, doSort);
	}
	
	public static void SetAtlas(ref Texture2D texture)
	{
		ShroomzSetTexture(texture.GetNativeTexturePtr());
	}

	public static void ToggleBlending(bool doBlending)
	{
		ShroomzToggleBlending(doBlending);
	}

	public static void SetTransform(ref Matrix4x4 worldMatrix, ref Matrix4x4 projectionMatrix)
	{
		ShroomzSetMatrices(ref worldMatrix, ref projectionMatrix);
	}

	public static void Render(ushort first, ushort count)
	{
		GL.IssuePluginEvent(((first << 16) | (count & 0xffff)));
	}

	public uint index
	{
		get { return idx; }
		set { idx = value; }
	}

	public float x
	{
		get { return data[idx * DATA_BLOCK]; }
		set { data[idx * DATA_BLOCK] = value; }
	}

	public float y
	{
		get { return data[idx * DATA_BLOCK + 1]; }
		set { data[idx * DATA_BLOCK + 1] = value; }
	}

	public float z
	{
		get { return data[idx * DATA_BLOCK + 2]; }
		set { data[idx * DATA_BLOCK + 2] = value; }
	}

	public float scaleX
	{
		get { return data[idx * DATA_BLOCK + 3]; }
		set { data[idx * DATA_BLOCK + 3] = value; }
	}

	public float scaleY
	{
		get { return data[idx * DATA_BLOCK + 4]; }
		set { data[idx * DATA_BLOCK + 4] = value; }
	}

	public float angle
	{
		get { return data[idx * DATA_BLOCK + 5]; }
		set { data[idx * DATA_BLOCK + 5] = value; }
	}

	public float red
	{
		get { return data[idx * DATA_BLOCK + 6]; }
		set { data[idx * DATA_BLOCK + 6] = value; }
	}

	public float green
	{
		get { return data[idx * DATA_BLOCK + 7]; }
		set { data[idx * DATA_BLOCK + 7] = value; }
	}

	public float blue
	{
		get { return data[idx * DATA_BLOCK + 8]; }
		set { data[idx * DATA_BLOCK + 8] = value; }
	}

	public float alpha
	{
		get { return data[idx * DATA_BLOCK + 9]; }
		set { data[idx * DATA_BLOCK + 9] = value; }
	}

	public float texX
	{
		get { return data[idx * DATA_BLOCK + 10]; }
		set { data[idx * DATA_BLOCK + 10] = value; }
	}

	public float texY
	{
		get { return data[idx * DATA_BLOCK + 11]; }
		set { data[idx * DATA_BLOCK + 11] = value; }
	}

	public float texWidth
	{
		get { return data[idx * DATA_BLOCK + 12]; }
		set { data[idx * DATA_BLOCK + 12] = value; }
	}

	public float texHeight
	{
		get { return data[idx * DATA_BLOCK + 13]; }
		set { data[idx * DATA_BLOCK + 13] = value; }
	}
	};

	public Texture2D shroomTex;

	private const RENDER_MODE mode = RENDER_MODE.DIRECT;
	private const bool blending = false;

	private const uint shroomzCount = 5;
	private Shroom[] shroomz;

	IEnumerator Start()
	{
		
		if (!Shroom.Initialize(shroomzCount, mode))
		{
			Debug.LogError("Can't initialize Shroomz!");
			yield break;
		}
		else
			Debug.Log("Shroomz Initialized");

		Debug.Log("Mode: " + Enum.GetName(typeof(RENDER_MODE), mode));
		Debug.Log("Blending: " + (blending ? "On" : "Off"));
		Debug.Log("Capacity: " + shroomzCount);
		
		shroomz = new Shroom[shroomzCount];
		
		for (uint i = 0; i < shroomzCount; ++i)
			shroomz[i].index = i;

		Shroom.SetAtlas(ref shroomTex);

		// If not using alpha test with 0.25 treshold.
		Shroom.ToggleBlending(blending);

		// This is the fastest way to call renderer plugin but Camera.OnPostRender is acceptable too.
		yield return StartCoroutine("CallPluginAtEndOfFrames");
	}

	void OnDestroy()
	{
		Shroom.Free();
		Debug.Log("Shroomz Freed");
	}

	private void UpdateAndRenderShroomz(Camera cam)
	{
		for (uint i = 0; i < shroomzCount; ++i)
		{
			shroomz[i].x = Mathf.Cos(Time.timeSinceLevelLoad + i) * 1.5F;
			shroomz[i].y = Mathf.Sin(Time.timeSinceLevelLoad - i) * 1.5F;
			shroomz[i].z = i * 0.001F;

			shroomz[i].scaleX = Mathf.Abs(Mathf.Cos(Time.timeSinceLevelLoad * i));
			shroomz[i].scaleY = shroomz[i].scaleX;

			shroomz[i].angle = 0;//Mathf.Sin(Time.timeSinceLevelLoad - i) * 360.0F * Mathf.Deg2Rad;								
		}

		// This is slow, use with care (not necessary to call every frame)!
		Shroom.Update(shroomzCount, true);

		Matrix4x4 world = cam.worldToCameraMatrix,
		proj = cam.projectionMatrix;
		
		// Must be set before draw.
		Shroom.SetTransform(ref world, ref proj);

		// Setup range of drawing elements.
		Shroom.Render(0, (ushort)shroomzCount);
	}

	// Sprites could be rendered using this method as well.
	/*void OnPostRender()
	{
		UpdateAndRenderShroomz(this.GetComponent<Camera>());
	}*/

	private IEnumerator CallPluginAtEndOfFrames()
	{
		Camera cam = Camera.main;

		while(true)
		{
			yield return new WaitForEndOfFrame();

			UpdateAndRenderShroomz(cam);
		}
	}
}
