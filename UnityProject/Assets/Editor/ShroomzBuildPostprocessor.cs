﻿using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System.IO;

public class ShroomzBuildPostprocessor
{
	[PostProcessBuild]
	public static void OnPostprocessBuild(BuildTarget target, string pathToBuiltProject)
	{
		if (target == BuildTarget.iOS)
			OnPostprocessBuildIOS(pathToBuiltProject);
	}

	private static void OnPostprocessBuildIOS(string pathToBuiltProject)
	{
		File.Copy("../ShroomzPlugin/UnityPluginInterface.h", Path.Combine(pathToBuiltProject, "Libraries/UnityPluginInterface.h"), true);
		File.Copy("../ShroomzPlugin/Shroomz.cpp", Path.Combine(pathToBuiltProject, "Libraries/Shroomz.cpp"), true);
	}
}
